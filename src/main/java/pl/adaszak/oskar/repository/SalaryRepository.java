package pl.adaszak.oskar.repository;

import org.springframework.data.repository.CrudRepository;
import pl.adaszak.oskar.model.WorkTimeData;

public interface SalaryRepository extends CrudRepository<WorkTimeData, Long> {
}
