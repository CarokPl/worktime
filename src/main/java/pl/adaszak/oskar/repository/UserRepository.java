package pl.adaszak.oskar.repository;

import org.springframework.data.repository.CrudRepository;
import pl.adaszak.oskar.model.UserData;

public interface UserRepository extends CrudRepository<UserData, String> {
	UserData findByLogin(String login);
}
