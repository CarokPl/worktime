package pl.adaszak.oskar.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.adaszak.oskar.model.UserData;
import pl.adaszak.oskar.service.UserService;

@Controller
public class UserController {

	private final UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping("/user/add")
	public String addUser(Model model) {
		model.addAttribute("userData", new UserData());
		return "addUser";
	}

	@PostMapping("/user/add")
	public String addUserSubmit(@ModelAttribute UserData userData) {
		System.out.println("userData = [" + userData + "]");
		userService.createUser(userData);

		return "addUserResult";
	}

	@GetMapping("/user/login")
	public String loginUser() {
		return "login";
	}
}