package pl.adaszak.oskar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.adaszak.oskar.model.WorkTimeData;
import pl.adaszak.oskar.service.SalaryService;

@Controller
public class WorkTimeController {

	private final SalaryService salaryService;

	@Autowired
	public WorkTimeController(SalaryService salaryService) {
		this.salaryService = salaryService;
	}

	@GetMapping("/workTime")
	public String setWorkTime(Model model) {
		model.addAttribute("workTimeData", new WorkTimeData());
		return "workTime";
	}

	@PostMapping("/workTime")
	public String setWorkTimeSubmit(@ModelAttribute WorkTimeData workTimeData, Model modal) {
		workTimeData = salaryService.calculateSalary(workTimeData);
		salaryService.saveSalary(workTimeData);
		modal.addAttribute("workTime", workTimeData);
		return "workTimeSubmit";
	}
}
