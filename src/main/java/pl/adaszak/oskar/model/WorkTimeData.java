package pl.adaszak.oskar.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class WorkTimeData {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private int preparationOfMaterials;
	private int trainingX;
	private int trainingY;
	private int delegation;
	private String month;
	@OneToOne(cascade = CascadeType.ALL)
	private Salary salary;
	private String user;

	public WorkTimeData() {
	}

	public int getPreparationOfMaterials() {
		return preparationOfMaterials;
	}

	public void setPreparationOfMaterials(int preparationOfMaterials) {
		this.preparationOfMaterials = preparationOfMaterials;
	}

	public int getTrainingX() {
		return trainingX;
	}

	public void setTrainingX(int trainingX) {
		this.trainingX = trainingX;
	}

	public int getTrainingY() {
		return trainingY;
	}

	public void setTrainingY(int trainingY) {
		this.trainingY = trainingY;
	}

	public int getDelegation() {
		return delegation;
	}

	public void setDelegation(int delegation) {
		this.delegation = delegation;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Salary getSalary() {
		return salary;
	}

	public void setSalary(Salary salary) {
		this.salary = salary;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, preparationOfMaterials, trainingX, trainingY, delegation, month, salary, user);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final WorkTimeData other = (WorkTimeData) obj;
		return Objects.equals(this.id, other.id)
				&& Objects.equals(this.preparationOfMaterials, other.preparationOfMaterials)
				&& Objects.equals(this.trainingX, other.trainingX)
				&& Objects.equals(this.trainingY, other.trainingY)
				&& Objects.equals(this.delegation, other.delegation)
				&& Objects.equals(this.month, other.month)
				&& Objects.equals(this.salary, other.salary)
				&& Objects.equals(this.user, other.user);
	}

	@Override
	public String toString() {
		return "WorkTimeData{" +
				"id=" + id +
				", preparationOfMaterials=" + preparationOfMaterials +
				", trainingX=" + trainingX +
				", trainingY=" + trainingY +
				", delegation=" + delegation +
				", month='" + month + '\'' +
				", salary=" + salary +
				", user='" + user + '\'' +
				'}';
	}
}
