package pl.adaszak.oskar.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;


@Entity
public class Salary {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Float brutton;
	private Float netto;

	public Salary(Float brutton, Float netto) {
		this.brutton = brutton;
		this.netto = netto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setBrutton(Float brutton) {
		this.brutton = brutton;
	}

	public void setNetto(Float netto) {
		this.netto = netto;
	}

	public Float getBrutton() {
		return brutton;
	}

	public Float getNetto() {
		return netto;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, brutton, netto);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final Salary other = (Salary) obj;
		return Objects.equals(this.id, other.id)
				&& Objects.equals(this.brutton, other.brutton)
				&& Objects.equals(this.netto, other.netto);
	}

	@Override
	public String toString() {
		return "Salary{" +
				"id=" + id +
				", brutton=" + brutton +
				", netto=" + netto +
				'}';
	}
}
