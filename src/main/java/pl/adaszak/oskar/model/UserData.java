package pl.adaszak.oskar.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;


@Entity
public class UserData {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String login;
	private String password;
	private boolean admin;
	private int preparationOfMaterials;
	private int trainingX;
	private int trainingY;
	private int delegation;

	public UserData() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public int getPreparationOfMaterials() {
		return preparationOfMaterials;
	}

	public void setPreparationOfMaterials(int preparationOfMaterials) {
		this.preparationOfMaterials = preparationOfMaterials;
	}

	public int getTrainingX() {
		return trainingX;
	}

	public void setTrainingX(int trainingX) {
		this.trainingX = trainingX;
	}

	public int getTrainingY() {
		return trainingY;
	}

	public void setTrainingY(int trainingY) {
		this.trainingY = trainingY;
	}

	public int getDelegation() {
		return delegation;
	}

	public void setDelegation(int delegation) {
		this.delegation = delegation;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, login, password, admin, preparationOfMaterials, trainingX, trainingY, delegation);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final UserData other = (UserData) obj;
		return Objects.equals(this.id, other.id)
				&& Objects.equals(this.login, other.login)
				&& Objects.equals(this.password, other.password)
				&& Objects.equals(this.admin, other.admin)
				&& Objects.equals(this.preparationOfMaterials, other.preparationOfMaterials)
				&& Objects.equals(this.trainingX, other.trainingX)
				&& Objects.equals(this.trainingY, other.trainingY)
				&& Objects.equals(this.delegation, other.delegation);
	}

	@Override
	public String toString() {
		return "UserData{" +
				"id='" + id + '\'' +
				", login='" + login + '\'' +
				", password='" + password + '\'' +
				", admin=" + admin +
				", preparationOfMaterials='" + preparationOfMaterials + '\'' +
				", trainingX='" + trainingX + '\'' +
				", trainingY='" + trainingY + '\'' +
				", delegation='" + delegation + '\'' +
				'}';
	}
}
