package pl.adaszak.oskar.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import pl.adaszak.oskar.model.UserData;
import pl.adaszak.oskar.repository.UserRepository;

import java.util.Optional;

@Component
public class UserService {

	private final UserRepository userRepository;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	public UserService(UserRepository userRepository,
	                   BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepository = userRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	public void createUser(UserData userData) {
		userData.setPassword(bCryptPasswordEncoder.encode(userData.getPassword()));
		userRepository.save(userData);
	}

	public String findLoggedInUsername() {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return Optional.ofNullable(user).map(User::getUsername).orElse(null);
	}
}
