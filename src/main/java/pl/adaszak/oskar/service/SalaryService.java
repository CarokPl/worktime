package pl.adaszak.oskar.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.adaszak.oskar.model.Salary;
import pl.adaszak.oskar.model.UserData;
import pl.adaszak.oskar.model.WorkTimeData;
import pl.adaszak.oskar.repository.SalaryRepository;
import pl.adaszak.oskar.repository.UserRepository;

import java.util.Optional;

@Service
public class SalaryService {

	private final UserRepository userRepository;
	private final UserService userService;
	private final SalaryRepository salaryRepository;

	@Autowired
	public SalaryService(UserRepository userRepository,
	                     UserService userService,
	                     SalaryRepository salaryRepository) {
		this.userService = userService;
		this.userRepository = userRepository;
		this.salaryRepository = salaryRepository;
	}

	public WorkTimeData calculateSalary(WorkTimeData workTimeData) {
		String username = userService.findLoggedInUsername();
		UserData userData = userRepository.findByLogin(username);
		Optional.ofNullable(userData).orElseThrow(() -> new UsernameNotFoundException(username));

		Float brutto = 0F;
		brutto += workTimeData.getDelegation() * userData.getDelegation();
		brutto += workTimeData.getPreparationOfMaterials() * userData.getPreparationOfMaterials();
		brutto += workTimeData.getTrainingX() * userData.getTrainingX();
		brutto += workTimeData.getTrainingY() * userData.getTrainingY();

		Float netto = brutto - 0.23F * brutto;
		workTimeData.setSalary(new Salary(brutto, netto));
		workTimeData.setUser(username);
		return workTimeData;
	}

	public void saveSalary(WorkTimeData workTimeData) {
		salaryRepository.save(workTimeData);
	}
}
